# CopperCAM Post Processors

We created custom post processors for some of the machines we use for PCB milling at the Aalto Fablab.

1. [CopperCAM_MDX-40_NC.ppf](CopperCAM_MDX-40_NC.ppf)
1. [CopperCAM_SRM-20_NC.ppf](CopperCAM_SRM-20_NC.ppf)

## Motivation

We have the following Roland machines at the lab.

1. Roland MDX-40
2. Roland SRM-20

There were several problems with the CopperCAM built-in post processors for the roland Roland machines we have.

1. File extensions of the NC code files were not recognized by Roland VPanel software.
1. There was no post processor that would take advantage of the NC code setting in Roland VPanel software.

## Installation

Download the post processor you need and open CopperCAM as an Administrator.

1. Go to **Parameters > Output data format...**
1. Load the post processor from there.

Enjoy!

